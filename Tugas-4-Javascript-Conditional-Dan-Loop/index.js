//Soal 1
var nilai = 70;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai <= 85) {
  console.log("B");
} else if (nilai >= 65 && nilai <= 75) {
  console.log("C");
} else if (nilai < 55) {
  console.log("D");
}

// Soal 2
var tanggal = 03;
var bulan = 7;
var tahun = 1997;

switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
  default:
}
console.log(tanggal + " " + bulan + " " + tahun);

//soal 3
var n = "7";
var result = "";
for (var i = 1; i <= n; i++) {
  result += "#";
  console.log(result);
}

// Soal 4
var result = "";
var count = 0;
var separator = "";
for (var i = 1; i <= 10; i++) {
  count++;
  if (count % 3 == 0) {
    separator += "###";
    result = i + " - I love VueJS" + "\n" + separator;

    count = 0;
  } else if (count % 2 == 1) {
    result = i + " - I love programming";
  } else if (count % 2 == 0) {
    result = i + " - I love javascript";
  }
  console.log(result);
}
