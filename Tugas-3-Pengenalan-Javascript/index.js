//Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var soal1 =
  pertama.slice(0, 4) +
  pertama.slice(11, 18) +
  " " +
  kedua.slice(0, 7) +
  " " +
  kedua.slice(8, 18).toUpperCase();

console.log("soal 1: " + soal1);

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var soal2 =
  parseInt(kataPertama) +
  parseInt(kataKedua) * parseInt(kataKetiga) +
  parseInt(kataKeempat);
console.log("soal 2: " + soal2);

//Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
