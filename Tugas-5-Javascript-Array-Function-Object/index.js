// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (let i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan[i]);
}

// Soal 2
function introduce(data) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " Tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobbi yaitu " +
    data.hobby
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// Soal 3
var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

function hitung_huruf_vokal(string) {
  return string.match(/[aeiou]/gi).length;
}

console.log(hitung_1, hitung_2); // 3 2

// Soal 4
// Maaf saya kurang ngerti dengan perintah soalnya 🙏🙏
function hitung(int) {
  return int - 2;
}
console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
