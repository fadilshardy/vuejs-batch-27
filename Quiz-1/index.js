// Soal Quiz 1

var tanggal = 28;
var bulan = 2;
var tahun = 2021;

function next_date(tanggal, bulan, tahun) {
  var date = new Date(tahun + "-" + bulan + "-" + tanggal);

  date.setDate(date.getDate() + 1);

  var month = date.toLocaleString("id-ID", { month: "long" });

  var tomorrow = date.getDate() + " " + month + " " + date.getFullYear();

  console.log(tomorrow);
  return tomorrow;
}

next_date(tanggal, bulan, tahun); // output : 1 Maret 2021

// Soal Quiz 2
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";

var kalimat_2 = "Saya Iqbal";

function jumlah_kata(string) {
  var wordCount = string.match(/(\w+)/g).length;

  console.log(wordCount);

  return wordCount;
}
jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2
