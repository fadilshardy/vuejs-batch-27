// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], (sisaWaktu) => {
  readBooks(sisaWaktu, books[1], (sisaWaktu2) => {
    readBooks(sisaWaktu2, books[2], (sisaWaktu3) => {
      readBooks(sisaWaktu3, books[3], (sisaWaktu4) => {});
    });
  });
});
